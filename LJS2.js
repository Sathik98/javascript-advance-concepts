      //JavaScript ARRAYS
      /*
      const house = ["living room", "kitchen", "bed room"];
      house[3] = "bathroom";  //accessing array elements.
      house[0] = "Hall";  //changing an array elements.

      console.log(house.length);
      console.log(typeof house);
      console.log(Array.isArray(house));
      console.log(house instanceof Array);
      console.log(house);
      */
    
      /*
      const man = ["head", "body", "hand", "leg"];
      
      console.log(man);
      console.log(man.toString());
      console.log(man.join(" - "));
      console.log(man.pop());            //popping pop()
      console.log(man);
      */
      
      /*
      function myFunction() {
          man.push("finger");               //pushing push()
          console.log(man);
          console.log(man.push("finger"));
      }
      */

      /*
       console.log(man.shift());            //shifting
       console.log(man); 

       console.log(man.unshift("hair"));   //unshifting
       console.log(man);
       */

       /*
       const fruits= ["apple", "banana"];

       console.log("the fruits is: " + fruits[0] + " " + fruits[1]);
       delete fruits[0];                                              //delete
       console.log("the fruits is: " + fruits[0] + " " + fruits[1]);
       */
       
       /*
       const fruits = ["apple", "banana", "orange",];

       console.log(fruits);
       console.log(fruits.splice(2, 1, "grapes", "mango"));    //splice
       console.log(fruits);
       */
       //array reordering methods

       /*
       const fruits = ["banana", "orange", "mango", "apple"];
       
       console.log(fruits);
       console.log(fruits.sort());   //sorting
       console.log(fruits.reverse());  //reversing
       */
       /*
       var myMeals = ["dinner", "lunch", "breakfasst"];
       var schoolstd = [5, 4, 3, 2, 1];

       console.log(myMeals);
       console.log(schoolstd);

       myMeals.reverse();
       schoolstd.reverse();

       console.log(myMeals);
       console.log(schoolstd);

       var averageMarkList = [35, 45, 50, 65, 10, 99];
       averageMarkList.sort();
       console.log(averageMarkList);

       averageMarkList.push(100);
       averageMarkList.sort();
       console.log(averageMarkList);
       */
/*
       function compare(value1, value2) {
           console.log("value1: ", value1);
           console.log("value2: ", value2);
           if (value1 > value2) {
               return 1;
           } else if (value1 < value2) {
               return -1;
           } else {
               return 0;
           }
       }

       averageMarkList.sort(compare);
       console.log(averageMarkList);
*/
        /*
        function ascendingOrder(value1, value2) {   //ascending order 
            return value1 - value2;
        }

        averageMarkList.sort(ascendingOrder);
        console.log(averageMarkList);

        function descendingOrder(value1, value2) {  //descending order
            if (value1 > value2) {
                return -1;
            } else if (value1 < value2) {
                return 1;
            } else {
                return 0;
            }
        }

        averageMarkList.sort(descendingOrder);
        console.log(averageMarkList);
        */
/*
        //array iteration methods
        //array.forEach()
        var availableFruits = ["apple", "orange", "mango", "banana"];

        availableFruits.forEach(function(value, index, array) {
            console.log("in this fruits array " + array);
            console.log("fruits " + value + " is at position " + index);
        })

        //array.map()
        var priceList = [10, 50, 100, 250, 500, 1000];
        var discount = 10;
        var newPriceList = priceList.map(function(price) {
            return price - (price * 10 / 100);
        });

        console.log(newPriceList);

        //array.filter()
        var employee = [
        {
            role: "trainee",
            salary: 15000 
        },
        {
            role: "junior dev",
            salary: 35000
        },
        {
            role: "senior dev",
            salary: 50000
        },
        {
            role: "team lead",
            salary: 60000
        },
        {
            role: "manager",
            salary: 75000
        },
        {
            role: "ceo",
            salary: 100000
        }
    ];

    var highSalaryEmployees = employee.filter(function(employee) {
        return employee.salary >= 50000;
    });

    console.log(highSalaryEmployees);

*/

   // date methods in js
/*
   var now = new Date();
   console.log(now);

   var dob = new Date("12, july 1998");
   console.log(dob);
   console.log(dob.valueOf());
   */

   //math methods in js
   /*
   var maxValue = Math.max(1, 3, 7, 8, 6, 37, 76, 87, 123, 786, 544, 8678, 65);
   console.log("Maximum Value: ", maxValue);

   var minValue = Math.min(7, 37, 76, 87, 123, 786, 544, 8678, 65);
   console.log("Minimum Value: ", minValue);

   console.log(Math.round(64.98));  //rounded method
   console.log(Math.ceil(7.1));  //ceil method
   console.log(Math.floor(8.9)); //floor method

   console.log(Math.random());    // random methods
   console.log(Math.random());
   console.log(Math.random());
  
   //example
   var randomWeek = Math.floor(Math.random() * numberOfChoices + firstValue);
   var numberOfChoices = 7;
   var firstValue = 1;
   var randomNumber = Math.floor(Math.random() * numberOfChoices + firstValue);

   console.log("Random Number: ", randomNumber);
   */

    //for of and for in loop
/*
   const person = {
       name: "sathik",
       age: 23
   };

   for(let x in person) {
       console.log(x + ": ", person[x]);
   }
*/

   //iterate
   //iterables over string

/*
   const name = "sathik";

   for (let x of name) {
       console.log("iterate: ", x);
   }
*/
      //iterables over array
/*
      const name = ["a", "l", "i"];

      for (let x of name) {
          console.log("iterate: ", x);
      }
*/

    //iterate over a set
/*
    const name = new Set(["s", "a", "t", "h", "i", "k"]);

    for (let x of name) {
        console.log("iterate: ", x);
    }
*/

    //iterate over map
/*
    const fruits = new Map([
        ["apple", 250],
        ["banana", 200],
        ["mango", 300],
    ]);

    for (let x of fruits) {
        console.log("iterate: ", x);
    }
*/

  // js sets
  // add values
  /*
  let letters = new Set ();

  letters.add("a");
  letters.add("b");
  letters.add("c");
  letters.add("d");
{
  console.log(letters);
}
*/
  // add variables
/*
  let letters = new Set();

  //add variables to the set
  const a = "a";
  const b = "b";
  const c = "c";
  const d = "d";

  //add values to the set
  letters.add("a");
  letters.add("b");
  letters.add("c");
  letters.add("d");

  {
    console.log(letters);
    console.log(letters.size);
  }
  */

  //add() method
/*
  let letters = new Set (["a", "b", "c"]);

  //add new elements
  letters.add("d");
  letters.add("e");
  letters.add("e");
  letters.add("e");

{
  console.log(letters);
}
*/

//forEach() method
/*
const letters = new Set(["a", "b", "c",]);

letters.forEach(function(value) {
    console.log(value);
})
*/

 //value() method
/*
 const letters = new Set(["a", "b", "c"]);

 for (let x of letters.values()) {
     console.log(letters);
 }
 */

 //maps in js
 // set and get
 /*
 const fruits = new Map();

 fruits.set("apple", 250);
 fruits.set("banana", 150);
 fruits.set("mango", 200);

 fruits.delete("mango");   //delete

 {
     console.log(fruits);
     console.log(fruits.size);       //size
     console.log(fruits.has("mango"));    //has
 }
 */

 //bitwise operators
/*
 const readPermission = 4;
 const writePermission = 2;
 const executePermission = 1;

 let myPermission = 0;

 myPermission = myPermission | readPermission;
 
 let message = (myPermission & readPermission) ? "yes" : "no";

 console.log (message);
 

 console.log(5 & 1);
 */

 //regular expression
/*
 let name = "sathik ali";
 
 console.log(name.search("ali"));  // search()

 let greetings = "Good Morning";

 console.log(greetings.replace("Good", "Code"));  //replace()

 let message = "hello! how are you?";
 const pattern = /y/;

 console.log(pattern.test(message));  //test()

 let message2 = "hi! i'm fine";
 const pattern2 = /f/;

 console.log(pattern2.exec(message2));  //exec()
 */

 //Errors 
/*
 try {
     aleert("welcome guest");
 }
 catch(err) {
     console.log(err.message);  //try and catch
 }
*/

 //scope
 //local scope
 /*
 myFunction();

 function myFunction () {
     let carName1 = "bmw";
     let carName2 = "audi";
     console.log(typeof carName1 + " " + carName1); //local variables declared inside function
 }

 console.log(typeof carName2);   //local variables declared outside function
*/

 //function scope
 /*
 function myFunction () {
     let carName = "swift";
 }

 
 function myFunction () {
    var carName = "swift";
}


function myFunction () {
    const carName = "swift";  //all three are same when declared inside a function
}
*/

//global scope
/*
let carName = "volvo";

myFunction();

function myFunction () {
    console.log("i can display " + carName);  // normal global variable
}
*/

/*
myFunction();
console.log("i can display " + carName);  //automaically global variable

function myFunction () {
     carName = "alto";
}
*/

//global variable in html
/*
var carName = "audi";
console.log("i can display " + window.carName); //global variable by var keyword

let carName1 = "toyoto";
console.log("i cannot display " + window.carName1); //global variable by let keyword
*/

//javascript Hoisting
/*
addNoDeclaration();

function addNoDeclaration () {
    console.log("adding no from function  declaration"); 
}


addNoExpression();

let addNoExpression = function () {
    console.log("adding no from function  expression");
};
*/

//strict mode
/*
"use strict";
x = {
    p1: 10,
    p2: 20
};

console.log (x);
*/

//this keyword
/*
const person = {
    fName: "sathik",
    lName: "ali",
    age: 23,

    fullName: function () {
        return "Hi! i am " + this.fName + " " + this.lName;
    },

    howOld: function () {
        return this.age + " years old";
    }
};

console.log(person.fullName() , person.howOld());
*/

//arrow function
/*
var hello;

hello = function () {
    return "Hello World!";
}
console.log (hello());  //normanl function syntax


hello = () => {
    return "Hello India!";
}
console.log(hello());  //shorter function syntax

hello = (msg) => "Hello TamilNadu! " + msg;
console.log(hello("tamil nadu is beautiful state in india"));   //more shorter function syntax if the function has only one statements
*/

//JS Classes
/*
class car {
    constructor(name, year) {
        this.name = name;
        this.year = year;
    }
}

const myCar = new car("audi", 2020);

console.log(myCar.name + " " + myCar.year);
*/

//eg:-
/*
class car {
    constructor(name,year) {
        this.name = name;
        this.year = year;
    }

    age() {
        let date = new Date();
        return date.getFullYear() - this.year;
    }
}

let myCar = new car("BMW ", 2020);

console.log("my car is " + myCar.age() + " years old.");
*/

//JS JSON
/*
let text = '{ "employees" : [' +
    '{ "firstName":"sathik" , "lastName":"ali"}, ' +
    '{ "firstName":"jake" , "lastName":"sully"}, ' +
    '{ "firstName":"toruk" , "lastName":"makto"}]} ';

    const obj = JSON.parse(text);

    console.log(obj.employees[0].firstName + " " + obj.employees[0].lastName);

*/








